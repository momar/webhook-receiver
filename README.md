# webhook-receiver

This Python application provides a webserver that executes a command every time a specific path has been accessed.

**Basic usage** - the first command will respond to http://localhost:8080/hello-world, the second one to http://localhost:8080/hello?name=Moritz:
```bash
WEBHOOK_PATH=/hello-world ./webhook-receiver echo "Hello World"
WEBHOOK_ALLOW_ENV=name WEBHOOK_PATH=/hello ./webhook-receiver sh -c 'echo "Hello $name"'
```

**Intended use case** - this server is intended to be used behind a load balancer that will proxy only the intended requests to webhook-receiver. You can for example use [Traefik](https://containo.us/traefik/) for that use case, and create a Docker container with webhook-receiver; when using the "alpine" base image, all you need is to add the application to your image and run `apk add --no-cache python3`.

**Environment variables**
- `WEBHOOK_PATH`: path prefix to which the webhook responds (GET and POST requests are supported). You probably want to set this to a securely generated random string.
- `WEBHOOK_TIMEOUT`: timeout in seconds after which the script will be terminated (defaults to 55).
- `WEBHOOK_ALLOW_ENV`: space-separated list of allowed environment variables that will be passed to the script from the query string if given. Set to `*` to allow any query string parameter to be passed as an environment variable.
- `WEBHOOK_ALLOW_STDIN`: whether the request body for POST requests should be passed to the script through STDIN (defaults to false).
- `WEBHOOK_QUEUE`: specifies how to handle multiple simultaneous requests:
  - `all`: wait until the current request is done and then start another one
  - `one`: return a "429 Too Many Requests" error for all previously queued requests, and start another one if the current request is done
  - `kill`: kill the script (SIGTERM + SIGKILL after 5 seconds) and start a new one
  - `fail`: return a "429 Too Many Requests" error
  - `parallel`: just start another instance immediately (default)
- `WEBHOOK_QUEUE_TIMEOUT`: timeout in seconds after which the queued request will be cancelled and a "429 Too Many Requests" error will be returned (defaults to `2 * WEBHOOK_TIMEOUT`).
- `PORT`: port to listen on, defaults to `80` if the EUID is `0` (this is useful for Docker containers), or `8080` otherwise.

All environment variables will be passed to the script, together with the `SCRIPT_NAME` variable containing the requested path (excluding the `WEBHOOK_PATH` prefix), and the CGI variables `QUERY_STRING`, `REQUEST_URI` and `HTTP_HOST`.

<!-- The response will have one of the following HTTP status codes: -->

TODO:
- Add the queueing modes `kill` and `one`
- Add a label-mode, similar to [Ofelia](https://github.com/mcuadros/ofelia)

